package com.example.upooj.myapplication;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Timer;
import java.util.TimerTask;

import static android.provider.AlarmClock.EXTRA_MESSAGE;


public class MainActivity extends AppCompatActivity implements SensorEventListener {

    Button btnSetAlarm, btnView, btnBack;
    ToggleButton btnToggle;
    long time;
    //EditText editTextInput;
    TextView lblTextView;
    TimePicker alarmTimePicker;
    private  SensorManager mSensorManager;
    Sensor mAccelerometer, mGyroscope;
    double x,y,z,lx,ly,lz;
    double l, m;
    double lastTime = 0;
    double currentTime;
    boolean color = false;
    String input;
    public boolean isToggleChecked = false;
    android.support.constraint.ConstraintLayout layoutAlarm;

    List<String> lstInputData = new ArrayList<String>();

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
       String sensorName = event.sensor.getName();
        System.out.println("hi this is poo");
        layoutAlarm = findViewById(R.id.layoutAlarm);
        if (event.sensor.getType()==Sensor.TYPE_GYROSCOPE){
            l = event.values[2];
            System.out.println(l);
            if (l > 4f)
            {
                layoutAlarm.setBackgroundColor(Color.GREEN);
            }
           /*
            else
            {
                layoutAlarm.setBackgroundColor(Color.YELLOW);
            }
            */
        }
        if (event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){
            x=event.values[0];
            y=event.values[1];
            z=event.values[2];
            currentTime = System.currentTimeMillis();
                if (currentTime - lastTime > 200) {
                    double timediff = currentTime - lastTime;
                    double speed = Math.abs(x + y + z - lx - ly - lz) / timediff * 1000;
                    if (speed > 30)
                    {
                        Intent intent = new Intent(this, Alarm.class);
                        PendingIntent sender = PendingIntent.getBroadcast(this, 0, intent, 0);
                        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                        alarmManager.cancel(sender);
                        btnToggle = (ToggleButton) findViewById(R.id.btnToggle);
                        btnToggle.setChecked(false);
                    }
                    /*if (speed > 30) {
                        if (color) {
                            lblTextView.setBackgroundColor(Color.GREEN);
                        } else {
                            lblTextView.setBackgroundColor(Color.DKGRAY);
                        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                        alertDialog.setMessage(input);
                        alertDialog.setTitle("Close your reminder for - ");
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener()
                                {
                                    public void onClick(DialogInterface dialog, int val)
                                    {
                                        dialog.cancel();
                                    }
                                });
                        alertDialog.show();
                        }
                        color = !color;
                    }*/
                    lastTime = currentTime;
                    lx = x;
                    ly = y;
                    lz = z;
                }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this,
                mAccelerometer,
                SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this,
                mGyroscope,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        currentTime = System.currentTimeMillis();
        lblTextView = (EditText) findViewById(R.id.lblTextView);
        btnToggle = (ToggleButton) findViewById(R.id.btnToggle);
        alarmTimePicker = findViewById(R.id.timePicker);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mSensorManager.registerListener(MainActivity.this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(MainActivity.this, mGyroscope, SensorManager.SENSOR_DELAY_NORMAL);

    }

    public void OnToggleClicked(View view)
    {
        input = lblTextView.getText().toString();
        AlarmManager objam =(AlarmManager) getSystemService(ALARM_SERVICE);
        Intent obji = new Intent(MainActivity.this, Alarm.class);
        obji.putExtra("inputKey", input);
        PendingIntent objpi = PendingIntent.getBroadcast(getApplicationContext(), 0, obji, PendingIntent.FLAG_UPDATE_CURRENT);

        if (((ToggleButton) view).isChecked())
        {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, alarmTimePicker.getCurrentHour());
            calendar.set(Calendar.MINUTE, alarmTimePicker.getCurrentMinute());
            time=(calendar.getTimeInMillis()-(calendar.getTimeInMillis()%60000));
            if(System.currentTimeMillis()>time)
            {
                if (calendar.AM_PM == 0)
                    time = time + (1000*60*60*12);
                else
                    time = time + (1000*60*60*24);
            }
            objam.setRepeating(AlarmManager.RTC_WAKEUP,time,5000 ,objpi);
            isToggleChecked = true;
            lstInputData.add(input);
        }
        else
        {
            objam.cancel(objpi);
            isToggleChecked = false;
        }
    }
}
