package com.example.upooj.myapplication;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

public class Alarm extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String objInput = intent.getStringExtra("inputKey");
        Toast.makeText(context,"Your reminder is set for - " + objInput, Toast.LENGTH_LONG).show();
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        MediaPlayer mp = MediaPlayer.create(context.getApplicationContext(), notification);
        mp.start();
        /*
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(objInput);
        alertDialog.setTitle("Close your reminder for - ");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int val)
                    {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
        */
    }
}
